using System.Collections.Generic;
using Weather.Db.CodeFirst;

namespace Weather.App.SupplierProviders
{
    public interface ISupplierProvider
    {
        string ProviderName { get; }

        /// <summary>
        /// Returns list of SupplierDataRecord for given city.
        /// <para />
        /// Returns null if something went wrong.
        /// </summary>
        List<SupplierDataRecord> GetSupplierByCity(City city);
    }
}