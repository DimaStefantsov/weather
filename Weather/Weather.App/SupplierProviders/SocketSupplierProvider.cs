using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Xml;
using Weather.App.Extensions;
using Weather.Db.CodeFirst;

namespace Weather.App.SupplierProviders
{
    public class SocketSupplierProvider : ISupplierProvider
    {
        #region Interface Members

        public string ProviderName
        {
            get { return "Socket Supplier Provider"; }
        }

        public List<SupplierDataRecord> GetSupplierByCity(City city)
        {
            string xml = GetXmlResponseFromService(city);
            var xmlDocument = GetXmlDocument(xml);
            if (xmlDocument == null)
            {
                return null;
            }
            else
            {
                var suppliersDataXml = xmlDocument.GetElementsByTagName("SupplierData");
                var result = GetSupplierDataRecords(city, suppliersDataXml);
                return result;
            }
        }

        #endregion

        #region Helpers

        private static string GetXmlResponseFromService(City city)
        {
            try
            {
                return GetXmlResponseFromServiceCanThrow(city);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong in socket GetXmlResponseFromServiceCanThrow(), proper debugging / logging required.");
                return "";
            }
        }

        private static string GetXmlResponseFromServiceCanThrow(City city)
        {
            using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                SendHttpPost(socket, city);
                string response = ReceiveHttpResponse(socket);
                string responseBody = GetResponseBody(response);
                return responseBody;
            }
        }

        private static void SendHttpPost(Socket socket, City city)
        {
            string postData =
                @"<?xml version=""1.0"" encoding=""utf-8""?>
                              <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
                                  <soap:Body>
                                      <GetSupplierByCity xmlns=""http://www.webservicex.net/"">
                                          <City>" + city.Name + @"</City>
                                      </GetSupplierByCity>
                                  </soap:Body>
                              </soap:Envelope>";
            byte[] postDataBytes = Encoding.ASCII.GetBytes(postData);
            socket.Connect("www.webservicex.net", 80);
            socket.Send(Encoding.ASCII.GetBytes("POST /medicareSupplier.asmx HTTP/1.1\n"));
            socket.Send(Encoding.ASCII.GetBytes("Host: www.webservicex.net\n"));
            socket.Send(Encoding.ASCII.GetBytes("Content-Type: text/xml\n"));
            socket.Send(Encoding.ASCII.GetBytes("Connection: Close\n"));
            socket.Send(Encoding.ASCII.GetBytes("SOAPAction: http://www.webservicex.net/GetSupplierByCity\n"));
            socket.Send(Encoding.ASCII.GetBytes("Content-Length: " + postDataBytes.Length + "\n"));
            socket.Send(Encoding.ASCII.GetBytes("\n"));
            socket.Send(postDataBytes);
        }

        private static string ReceiveHttpResponse(Socket socket)
        {
            var buffer = new byte[socket.ReceiveBufferSize];
            var sb = new StringBuilder();
            for (;;)
            {
                int count = socket.Receive(buffer);
                if (count <= 0)
                {
                    break;
                }
                sb.Append(Encoding.ASCII.GetString(buffer, 0, count));
            }
            return sb.ToString();
        }

        private static string GetResponseBody(string response)
        {
            const string separator = "\r\n\r\n";
            int separatorPosition = response.IndexOf(separator, StringComparison.Ordinal);
            string responseBody = response.Substring(separatorPosition + separator.Length);
            return responseBody;
        }

        private static XmlDocument GetXmlDocument(string xml)
        {
            try
            {
                var xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(xml);
                return xmlDocument;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        private List<SupplierDataRecord> GetSupplierDataRecords(City city, XmlNodeList suppliersData)
        {
            if (suppliersData == null)
            {
                return new List<SupplierDataRecord>();
            }

            var result = new List<SupplierDataRecord>(suppliersData.Count);
            foreach (XmlNode supplierData in suppliersData)
            {
                var supplierDataRecord = GetSupplierDataRecord(city, supplierData);
                result.Add(supplierDataRecord);
            }
            return result;
        }

        private SupplierDataRecord GetSupplierDataRecord(City city, XmlNode supplierData)
        {
            // We want it to throw in case of some weird SupplierNumber / Id.
            long id = long.Parse(supplierData.GetInnerTextOf("SupplierNumber"));
            return new SupplierDataRecord
                       {
                           Id = id,
                           CityId = city.Id,
                           Address1 = supplierData.GetInnerTextOf("Address1"),
                           Address2 = supplierData.GetInnerTextOf("Address2"),
                           CompanyName = supplierData.GetInnerTextOf("CompanyName"),
                           Description = supplierData.GetInnerTextOf("Description"),
                           IsSupplierParticipating = supplierData.GetInnerTextOf("IsSupplierParticipating"),
                           State = supplierData.GetInnerTextOf("State"),
                           Telephone = supplierData.GetInnerTextOf("Telephone"),
                           Zip = supplierData.GetInnerTextOf("Zip"),
                           ZipPlus4 = supplierData.GetInnerTextOf("ZipPlus4")
                       };
        }

        #endregion
    }
}