using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Xml;
using Weather.App.Extensions;
using Weather.Db.CodeFirst;

namespace Weather.App.SupplierProviders
{
    public class HttpPostSupplierProvider : ISupplierProvider
    {
        #region Interface Members

        public string ProviderName
        {
            get { return "HttpPost Supplier Provider"; }
        }

        public List<SupplierDataRecord> GetSupplierByCity(City city)
        {
            string xml = GetXmlResponseFromService(city);
            var xmlDocument = GetXmlDocument(xml);
            if (xmlDocument == null)
            {
                return null;
            }
            else
            {
                var suppliersDataXml = xmlDocument.GetElementsByTagName("SupplierData");
                var result = GetSupplierDataRecords(city, suppliersDataXml);
                return result;
            }
        }

        #endregion

        #region Helpers

        private static string GetXmlResponseFromService(City city)
        {
            try
            {
                return GetXmlResponseFromServiceCanThrow(city);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong in GetXmlResponseFromServiceCanThrow(), proper debugging / logging required.");
                return "";
            }
        }

        private static string GetXmlResponseFromServiceCanThrow(City city)
        {
            const string url = "http://www.webservicex.net/medicareSupplier.asmx";
            var client = new WebClient { Encoding = Encoding.UTF8 };
            client.Headers.Add(HttpRequestHeader.ContentType, "text/xml");
            client.Headers.Add("SOAPAction", "http://www.webservicex.net/GetSupplierByCity");
            string postData =
                @"<?xml version=""1.0"" encoding=""utf-8""?>
                  <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
                      <soap:Body>
                          <GetSupplierByCity xmlns=""http://www.webservicex.net/"">
                              <City>" + city.Name + @"</City>
                          </GetSupplierByCity>
                      </soap:Body>
                  </soap:Envelope>";
            byte[] postDataBytes = System.Text.Encoding.ASCII.GetBytes(postData);
            byte[] responseBytes = client.UploadData(url, "POST", postDataBytes);
            string xml = Encoding.ASCII.GetString(responseBytes);
            return xml;
        }

        private static XmlDocument GetXmlDocument(string xml)
        {
            try
            {
                var xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(xml);
                return xmlDocument;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        private List<SupplierDataRecord> GetSupplierDataRecords(City city, XmlNodeList suppliersData)
        {
            if (suppliersData == null)
            {
                return new List<SupplierDataRecord>();
            }

            var result = new List<SupplierDataRecord>(suppliersData.Count);
            foreach (XmlNode supplierData in suppliersData)
            {
                var supplierDataRecord = GetSupplierDataRecord(city, supplierData);
                result.Add(supplierDataRecord);
            }
            return result;
        }

        private SupplierDataRecord GetSupplierDataRecord(City city, XmlNode supplierData)
        {
            // We want it to throw in case of some weird SupplierNumber / Id.
            long id = long.Parse(supplierData.GetInnerTextOf("SupplierNumber"));
            return new SupplierDataRecord
                       {
                           Id = id,
                           CityId = city.Id,
                           Address1 = supplierData.GetInnerTextOf("Address1"),
                           Address2 = supplierData.GetInnerTextOf("Address2"),
                           CompanyName = supplierData.GetInnerTextOf("CompanyName"),
                           Description = supplierData.GetInnerTextOf("Description"),
                           IsSupplierParticipating = supplierData.GetInnerTextOf("IsSupplierParticipating"),
                           State = supplierData.GetInnerTextOf("State"),
                           Telephone = supplierData.GetInnerTextOf("Telephone"),
                           Zip = supplierData.GetInnerTextOf("Zip"),
                           ZipPlus4 = supplierData.GetInnerTextOf("ZipPlus4")
                       };
        }

        #endregion
    }
}