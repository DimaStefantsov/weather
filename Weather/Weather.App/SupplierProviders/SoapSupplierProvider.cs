using System.Collections.Generic;
using Weather.App.Extensions;
using Weather.App.MediCareSupplierService;
using Weather.Db.CodeFirst;

namespace Weather.App.SupplierProviders
{
    public class SoapSupplierProvider : ISupplierProvider
    {
        #region Fields

        private readonly MediCareSupplierSoapClient soapClient;

        #endregion

        #region Constructors

        public SoapSupplierProvider()
        {
            soapClient = new MediCareSupplierSoapClient("MediCareSupplierSoap");
        }

        #endregion

        #region Interface Members

        public string ProviderName
        {
            get { return "Soap Supplier Provider"; }
        }

        public List<SupplierDataRecord> GetSupplierByCity(City city)
        {
            SupplierDataList supplierDataList;
            bool success = soapClient.GetSupplierByCity(out supplierDataList, city.Name);
            if (success)
            {
                var supplierDataRecords = GetSupplierDataRecords(city, supplierDataList.SupplierDatas);
                return supplierDataRecords;
            }
            else
            {
                return null;
            }
        }

        #endregion

        #region Helpers

        private List<SupplierDataRecord> GetSupplierDataRecords(City city, SupplierData[] suppliersData)
        {
            if (suppliersData == null || suppliersData.Length == 0)
            {
                return new List<SupplierDataRecord>();
            }

            var result = new List<SupplierDataRecord>(suppliersData.Length);
            foreach (var supplierData in suppliersData)
            {
                var supplierDataRecord = GetSupplierDataRecord(city, supplierData);
                result.Add(supplierDataRecord);
            }
            return result;
        }

        private SupplierDataRecord GetSupplierDataRecord(City city, SupplierData supplierData)
        {
            // We want it to throw in case of some weird SupplierNumber / Id.
            long id = long.Parse(supplierData.SupplierNumber.TrimIfNotNull());
            return new SupplierDataRecord
                       {
                           Id = id,
                           CityId = city.Id,
                           Address1 = supplierData.Address1.TrimIfNotNull(),
                           Address2 = supplierData.Address2.TrimIfNotNull(),
                           CompanyName = supplierData.CompanyName.TrimIfNotNull(),
                           Description = supplierData.Description.TrimIfNotNull(),
                           IsSupplierParticipating = supplierData.IsSupplierParticipating.TrimIfNotNull(),
                           State = supplierData.State.TrimIfNotNull(),
                           Telephone = supplierData.Telephone.TrimIfNotNull(),
                           Zip = supplierData.Zip.TrimIfNotNull(),
                           ZipPlus4 = supplierData.ZipPlus4.TrimIfNotNull()
                       };
        }

        #endregion
    }
}