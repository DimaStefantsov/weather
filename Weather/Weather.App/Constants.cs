﻿namespace Weather.App
{
    public static class Constants
    {
        public const int SchedulerThreadIntervalMilliseconds = 20000;
        public const int TimerIntervalMilliseconds = 10000;
        public const int MainThreadSleepMaxMilliseconds = 2000;
    }
}
