﻿using System;
using System.Diagnostics;
using System.Timers;
using Weather.App.SupplierProviders;

namespace Weather.App.Logic
{
    public static class TimerLogic
    {
        private static int counter;
        private static Timer timer;

        private static void TimerTask(object sender, ElapsedEventArgs e)
        {
            SupplierLogic.UpdateSuppliersData(new SocketSupplierProvider(), "TIMER");
            SupplierLogic.UpdateSuppliersData(new HttpPostSupplierProvider(), "TIMER");
            SupplierLogic.UpdateSuppliersData(new SoapSupplierProvider(), "TIMER");
            counter++;
        }

        public static void StartTimer()
        {
            timer = new Timer(Constants.TimerIntervalMilliseconds);
            timer.Elapsed += (sender, e) =>
            {
                timer.Stop();
                try
                {
                    Console.WriteLine("TIMER task={0} started.", counter+1);
                    var sw = Stopwatch.StartNew();
                    TimerTask(sender, e);
                    sw.Stop();
                    Console.WriteLine("TIMER task={0} finished in {1}.\n", counter, sw.Elapsed.ToString());
                }
                finally
                {
                    timer.Start();
                }
            };
            timer.Start();
        }
    }
}
