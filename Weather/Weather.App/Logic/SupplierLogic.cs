﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Weather.App.SupplierProviders;
using Weather.Db.CodeFirst;

namespace Weather.App.Logic
{
    public static class SupplierLogic
    {
        public static void ActOnCommandLineArguments(string[] commandLineArguments)
        {
            if (commandLineArguments == null || commandLineArguments.Length == 0)
            {
                return;
            }

            using (var dblc = new DatabaseLogicContext())
            {
                foreach (string argument in commandLineArguments)
                {
                    if (argument.StartsWith("-"))
                    {
                        string cityName = argument.Substring(1);
                        dblc.RemoveCity(cityName);
                    }
                    else
                    {
                        dblc.AddCity(argument);
                    }
                }
            }
        }

        public static void PrintExistingCities()
        {
            using (var dblc = new DatabaseLogicContext())
            {
                var cities = dblc.GetCities();
                Console.WriteLine("Cities count={0}, list={1}.",
                                  cities.Count,
                                  string.Join(", ", cities.Select(x => x.Name)));
            }
        }

        public static void UpdateSuppliersData(ISupplierProvider supplierProvider, string label = "")
        {
            using (var dblc = new DatabaseLogicContext())
            {
                var cities = dblc.GetCities();
                foreach (var city in cities)
                {
                    var swDownload = Stopwatch.StartNew();
                    var suppliersData = supplierProvider.GetSupplierByCity(city);
                    swDownload.Stop();

                    var swProcess = Stopwatch.StartNew();
                    dblc.UpdateCitySuppliersData(city, suppliersData);
                    swProcess.Stop();

                    Console.WriteLine("{0}: City={1}, records={2}, downloaded in {3}, processed in {4}. Using {5}.",
                                      label,
                                      city.Name,
                                      (suppliersData ?? new List<SupplierDataRecord>()).Count,
                                      swDownload.Elapsed,
                                      swProcess.Elapsed,
                                      supplierProvider.ProviderName);
                }
            }
        }
    }
}
