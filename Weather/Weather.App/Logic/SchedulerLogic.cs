﻿using System;
using System.Diagnostics;
using Weather.App.Scheduler;
using Weather.App.SupplierProviders;

namespace Weather.App.Logic
{
    public static class SchedulerLogic
    {
        private static int counter;

        private static void ScheduledTask(IntervalTaskContext context)
        {
            SupplierLogic.UpdateSuppliersData(new SocketSupplierProvider(), "SCHEDULER");
            SupplierLogic.UpdateSuppliersData(new HttpPostSupplierProvider(), "SCHEDULER");
            SupplierLogic.UpdateSuppliersData(new SoapSupplierProvider(), "SCHEDULER");
            counter++;
        }

        public static void StartScheduler()
        {
            if (IntervalTask.Current != null)
            {
                return;
            }

            IntervalTask.CreateTask(context =>
            {
                Console.WriteLine("SCHEDULER task={0} started.", counter+1);
                var sw = Stopwatch.StartNew();
                ScheduledTask(context);
                sw.Stop();
                Console.WriteLine("SCHEDULED task={0} finished in {1}.\n", counter, sw.Elapsed.ToString());
            });

            IntervalTask.Current.SetTimerInterval(Constants.SchedulerThreadIntervalMilliseconds);
        }

        public static void DisposeScheduler()
        {
            if (IntervalTask.Current != null)
            {
                IntervalTask.Current.Dispose();
            }
        }
    }
}
