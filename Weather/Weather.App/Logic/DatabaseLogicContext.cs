﻿using System;
using System.Collections.Generic;
using System.Linq;
using Weather.Db.CodeFirst;
using Weather.Db.Context;

namespace Weather.App.Logic
{
    public class DatabaseLogicContext : IDisposable
    {
        #region Fields

        private readonly SupplierDb db;
        private readonly bool doNotDisposeInnerContext;

        #endregion

        #region Properties

        public SupplierDb DB
        {
            get { return db; }
        }

        #endregion

        #region Constructors

        public DatabaseLogicContext()
        {
            db = new SupplierDb();
        }

        /// <summary>
        /// Will not remove received db context on dblc.Dispose()
        /// </summary>
        /// <param name="db">Received db context.</param>
        public DatabaseLogicContext(SupplierDb db)
        {
            doNotDisposeInnerContext = true;
            this.db = db;
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            if (doNotDisposeInnerContext)
            {
                return;
            }

            db.Dispose();
        }

        #endregion

        #region City

        public List<City> GetCities()
        {
            return db.Cities.ToList();
        }

        public void AddCity(string cityName)
        {
            var existingCity = db.Cities.FirstOrDefault(x => x.Name == cityName);
            if (existingCity != null)
            {
                Console.WriteLine("City={0} already exists in DB.", cityName);
                return;
            }

            var city = new City {Name = cityName};
            db.Cities.Add(city);
            int changesCount = db.SaveChanges();
            Console.WriteLine("Added city={0}, changes count={1}.", cityName, changesCount);
        }

        public void RemoveCity(string cityName)
        {
            var cities = db.Cities
                .Where(x => x.Name == cityName)
                .ToList();
            if (cities.Count == 0)
            {
                Console.WriteLine("No such city={0} in DB.", cityName);
                return;
            }

            foreach (var city in cities)
            {
                db.Cities.Remove(city);
            }
            int changesCount = db.SaveChanges();
            Console.WriteLine("Removed cities, name={0}, changes count={1}.", cityName, changesCount);
        }

        #endregion

        #region UpdateCitySuppliersData

        public void UpdateCitySuppliersData(City city, List<SupplierDataRecord> suppliersData)
        {
            if (suppliersData == null)
            {
                Console.WriteLine("SuppliersData is null!");
                return;
            }

            var oldSuppliers = db.SupplierDataRecords
                .Where(x => x.CityId == city.Id)
                .ToList();
            var oldSuppliersIds = oldSuppliers.Select(x => x.Id).ToList();

            // Some records have same Id.
            var newSuppliers = suppliersData
                .GroupBy(x => x.Id)
                .Select(x => x.First())
                .ToList();
            var newSuppliersIds = newSuppliers.Select(x => x.Id).ToList();

            var idsToAdd = newSuppliersIds.Except(oldSuppliersIds).ToList();
            var suppliersToAdd = newSuppliers
                .Where(x => idsToAdd.Contains(x.Id))
                .ToList();
            AddSuppliers(suppliersToAdd);

            var idsToRemove = oldSuppliersIds.Except(newSuppliersIds).ToList();
            var suppliersToRemove = oldSuppliers
                .Where(x => idsToRemove.Contains(x.Id))
                .ToList();
            RemoveSuppliers(suppliersToRemove);

            var idsToUpdate = newSuppliersIds.Except(idsToAdd).ToList();
            var suppliersToUpdate = newSuppliers
                .Where(x => idsToUpdate.Contains(x.Id))
                .ToList();
            UpdateSuppliers(suppliersToUpdate);
        }

        private void AddSuppliers(List<SupplierDataRecord> suppliersToAdd)
        {
            foreach (var supplierDataRecord in suppliersToAdd)
            {
                db.SupplierDataRecords.Add(supplierDataRecord);
            }
            int changesCount = db.SaveChanges();
            if (changesCount > 0)
            {
                Console.WriteLine("Added {0} entities.", changesCount);
            }
        }

        private void RemoveSuppliers(List<SupplierDataRecord> suppliersToRemove)
        {
            foreach (var supplierDataRecord in suppliersToRemove)
            {
                db.SupplierDataRecords.Remove(supplierDataRecord);
            }
            int changesCount = db.SaveChanges();
            if (changesCount > 0)
            {
                Console.WriteLine("Removed {0} entities.", changesCount);
            }
        }

        private void UpdateSuppliers(List<SupplierDataRecord> suppliersToUpdate)
        {
            foreach (var supplierDataRecord in suppliersToUpdate)
            {
                var oldSupplier = db.SupplierDataRecords.Find(supplierDataRecord.Id);
                db.Entry(oldSupplier).CurrentValues.SetValues(supplierDataRecord);
            }
            int changesCount = db.SaveChanges();
            if (changesCount > 0)
            {
                Console.WriteLine("Updated {0} entities.", changesCount);
            }
        }

        #endregion
    }
}
