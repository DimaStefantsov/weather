﻿ using System;
 using System.Threading;
 using Weather.App.Logic;

namespace Weather.App
{
    class Program
    {
        static void Main(string[] args)
        {
            SupplierLogic.ActOnCommandLineArguments(args);
            SupplierLogic.PrintExistingCities();

            SchedulerLogic.StartScheduler();
            TimerLogic.StartTimer();

            var random = new Random();
            for (;;)
            {
                Console.WriteLine(DateTime.Now.ToString("HH:mm:ss.ffff"));
                Thread.Sleep(random.Next(Constants.MainThreadSleepMaxMilliseconds));
            }
        }
    }
}
