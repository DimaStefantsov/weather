﻿namespace Weather.App.Extensions
{
    public static class StringExtensions
    {
        public static string TrimIfNotNull(this string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return s;
            }
            else
            {
                return s.Trim();
            }
        }
    }
}
