﻿using System.Xml;

namespace Weather.App.Extensions
{
    public static class XmlNodeExtensions
    {
        /// <summary>
        /// Returns trimmed InnerText or null if node with such name is not found.
        /// </summary>
        public static string GetInnerTextOf(this XmlNode xmlNode, string elementName)
        {
            var innerElement = xmlNode[elementName];
            if (innerElement == null)
            {
                return null;
            }
            else
            {
                return innerElement.InnerText.Trim();
            }
        }
    }
}
