﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Weather.App.MediCareSupplierService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://www.webservicex.net/", ConfigurationName="MediCareSupplierService.MediCareSupplierSoap")]
    public interface MediCareSupplierSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.webservicex.net/GetSupplierByZipCode", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        bool GetSupplierByZipCode(out Weather.App.MediCareSupplierService.SupplierDataList SupplierDataLists, string zip);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.webservicex.net/GetSupplierByCity", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        bool GetSupplierByCity(out Weather.App.MediCareSupplierService.SupplierDataList SupplierDataLists, string City);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.webservicex.net/GetSupplierBySupplyType", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        bool GetSupplierBySupplyType(out Weather.App.MediCareSupplierService.SupplierDataList SupplierDataLists, string description);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.webservicex.net/")]
    public partial class SupplierDataList : object, System.ComponentModel.INotifyPropertyChanged {
        
        private SupplierData[] supplierDatasField;
        
        private int totalRecordsField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order=0)]
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable=false)]
        public SupplierData[] SupplierDatas {
            get {
                return this.supplierDatasField;
            }
            set {
                this.supplierDatasField = value;
                this.RaisePropertyChanged("SupplierDatas");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public int TotalRecords {
            get {
                return this.totalRecordsField;
            }
            set {
                this.totalRecordsField = value;
                this.RaisePropertyChanged("TotalRecords");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.webservicex.net/")]
    public partial class SupplierData : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string supplierNumberField;
        
        private string companyNameField;
        
        private string address1Field;
        
        private string address2Field;
        
        private string cityField;
        
        private string stateField;
        
        private string zipField;
        
        private string zipPlus4Field;
        
        private string telephoneField;
        
        private string descriptionField;
        
        private string isSupplierParticipatingField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string SupplierNumber {
            get {
                return this.supplierNumberField;
            }
            set {
                this.supplierNumberField = value;
                this.RaisePropertyChanged("SupplierNumber");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string CompanyName {
            get {
                return this.companyNameField;
            }
            set {
                this.companyNameField = value;
                this.RaisePropertyChanged("CompanyName");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string Address1 {
            get {
                return this.address1Field;
            }
            set {
                this.address1Field = value;
                this.RaisePropertyChanged("Address1");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string Address2 {
            get {
                return this.address2Field;
            }
            set {
                this.address2Field = value;
                this.RaisePropertyChanged("Address2");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string City {
            get {
                return this.cityField;
            }
            set {
                this.cityField = value;
                this.RaisePropertyChanged("City");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=5)]
        public string State {
            get {
                return this.stateField;
            }
            set {
                this.stateField = value;
                this.RaisePropertyChanged("State");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=6)]
        public string Zip {
            get {
                return this.zipField;
            }
            set {
                this.zipField = value;
                this.RaisePropertyChanged("Zip");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=7)]
        public string ZipPlus4 {
            get {
                return this.zipPlus4Field;
            }
            set {
                this.zipPlus4Field = value;
                this.RaisePropertyChanged("ZipPlus4");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=8)]
        public string Telephone {
            get {
                return this.telephoneField;
            }
            set {
                this.telephoneField = value;
                this.RaisePropertyChanged("Telephone");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=9)]
        public string Description {
            get {
                return this.descriptionField;
            }
            set {
                this.descriptionField = value;
                this.RaisePropertyChanged("Description");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=10)]
        public string IsSupplierParticipating {
            get {
                return this.isSupplierParticipatingField;
            }
            set {
                this.isSupplierParticipatingField = value;
                this.RaisePropertyChanged("IsSupplierParticipating");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface MediCareSupplierSoapChannel : Weather.App.MediCareSupplierService.MediCareSupplierSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class MediCareSupplierSoapClient : System.ServiceModel.ClientBase<Weather.App.MediCareSupplierService.MediCareSupplierSoap>, Weather.App.MediCareSupplierService.MediCareSupplierSoap {
        
        public MediCareSupplierSoapClient() {
        }
        
        public MediCareSupplierSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public MediCareSupplierSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MediCareSupplierSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MediCareSupplierSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public bool GetSupplierByZipCode(out Weather.App.MediCareSupplierService.SupplierDataList SupplierDataLists, string zip) {
            return base.Channel.GetSupplierByZipCode(out SupplierDataLists, zip);
        }
        
        public bool GetSupplierByCity(out Weather.App.MediCareSupplierService.SupplierDataList SupplierDataLists, string City) {
            return base.Channel.GetSupplierByCity(out SupplierDataLists, City);
        }
        
        public bool GetSupplierBySupplyType(out Weather.App.MediCareSupplierService.SupplierDataList SupplierDataLists, string description) {
            return base.Channel.GetSupplierBySupplyType(out SupplierDataLists, description);
        }
    }
}
