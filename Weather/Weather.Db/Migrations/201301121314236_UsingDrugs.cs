namespace Weather.Db.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UsingDrugs : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SupplierDataRecords",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CompanyName = c.String(),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        State = c.String(),
                        Zip = c.String(),
                        ZipPlus4 = c.String(),
                        Telephone = c.String(),
                        Description = c.String(),
                        IsSupplierParticipating = c.String(),
                        CityId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.CityId)
                .Index(t => t.CityId);
            
            DropTable("dbo.SuppliersData");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SuppliersData",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        SupplierNumber = c.String(),
                        CompanyName = c.String(),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Zip = c.String(),
                        ZipPlus4 = c.String(),
                        Telephone = c.String(),
                        Description = c.String(),
                        IsSupplierParticipating = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropIndex("dbo.SupplierDataRecords", new[] { "CityId" });
            DropForeignKey("dbo.SupplierDataRecords", "CityId", "dbo.Cities");
            DropTable("dbo.SupplierDataRecords");
        }
    }
}
