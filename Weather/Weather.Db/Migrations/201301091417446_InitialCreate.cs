namespace Weather.Db.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Towns",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WeatherRecords",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DateTime = c.DateTime(nullable: false),
                        Temperature = c.Int(nullable: false),
                        TownId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Towns", t => t.TownId)
                .Index(t => t.TownId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.WeatherRecords", new[] { "TownId" });
            DropForeignKey("dbo.WeatherRecords", "TownId", "dbo.Towns");
            DropTable("dbo.WeatherRecords");
            DropTable("dbo.Towns");
        }
    }
}
