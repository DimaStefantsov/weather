using System.Data.Entity.Migrations;

namespace Weather.Db.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Weather.Db.Context.SupplierDb>
    {
        // Add-Migration MyMigrationName

        // Update-Database -Script -SourceMigration:InitialCreate (last migration that is already applied)
        // then add [dbo.] like this:
        // CREATE TABLE [dbo].[Articles] (
        //
        // or
        //
        // Edit migration file by hand (add dbo.): 
        // public override void Up(){CreateTable("dbo.UploadedFiles"
        // public override void Down(){DropTable("dbo.UploadedFiles"
        // and then can use automatic thing:
        // Update-Database (localhost)
        // Update-Database -ConnectionProviderName "System.Data.SqlClient" -ConnectionString "Data Source=mssql03.infobox.ru;Initial Catalog=z163862_dota2cw.ru;User ID=z163862_mssql;Password=qweqwe"



        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Weather.Db.Context.SupplierDb context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
