namespace Weather.Db.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ClientSetId : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.SupplierDataRecords", "Id", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.SupplierDataRecords", "Id", c => c.Long(nullable: false, identity: true));
        }
    }
}
