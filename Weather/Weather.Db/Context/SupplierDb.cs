﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Weather.Db.CodeFirst;

namespace Weather.Db.Context
{
    // You can connect SSMS 2005/2008 to a LocalDB (2012) instance using named pipes.
    //
    // 1. Get the address of a (localdb) instance by running the following command:
    // "C:\Program Files\Microsoft SQL Server\110\Tools\Binn\SqlLocalDB.exe" info [InstanceName]
    // (if its the default instance you're interested in, specify v11.0 as the [InstanceName]
    //
    // 2. Copy the value returned in the "Instance pipe name", e.g. np:\\.\pipe\LOCALDB#13E05E01\tsql\query
    //
    // 3. Add a new server registration in SSMS and paste the "Instance pipe name" value as the Server Name.
    // NOTE: It is not necessary to specify "Named pipes" as the protocol. SSMS should detect the appropriate protocol for you.

    //Database.SetInitializer(new DropCreateDatabaseAlways<SupplierDb>());

    public class SupplierDb : DbContext // Automatically hooked to connection string named same as context class name.
    {
        public DbSet<City> Cities { get; set; }
        public DbSet<SupplierDataRecord> SupplierDataRecords { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}

