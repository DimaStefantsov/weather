﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Weather.Db.CodeFirst
{
    public class SupplierDataRecord
    {
        /// <summary>
        /// Assuming each city supplier is unique. Two cities can't have same supplier.
        /// Otherwise should have used both unique Id and SupplierNumber fields.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        public string CompanyName  { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public string ZipPlus4 { get; set; }

        public string Telephone { get; set; }

        public string Description { get; set; }

        public string IsSupplierParticipating { get; set; }

        public int CityId { get; set; }
        public virtual City City { get; set; }
    }
}
