﻿using System.ComponentModel.DataAnnotations;

namespace Weather.Db.CodeFirst
{
    public class City
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MinLength(1)]
        [MaxLength(255)]
        public string Name { get; set; }
    }
}
