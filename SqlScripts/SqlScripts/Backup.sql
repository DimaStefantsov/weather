declare @CommitName nvarchar(200)
declare @DiskName nvarchar(200)


set @CommitName = '01 name' --CHANGE NAME!


set @DiskName = N'd:\!SqlRevision\' + @CommitName + '.bak'
backup database [supplier] to
disk = @DiskName with compression, format, init,
name = @CommitName,
skip, norewind, nounload, stats = 10
go
