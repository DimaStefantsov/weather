alter database [supplier] set offline with rollback immediate
alter database [supplier] set online
go


EXEC msdb.dbo.sp_delete_database_backuphistory @database_name = N'supplier'
GO
USE [master]
GO
ALTER DATABASE [supplier] SET  SINGLE_USER WITH ROLLBACK IMMEDIATE
GO
USE [master]
GO


DROP DATABASE [supplier]
GO
